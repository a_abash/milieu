// Webpack v4
const path = require('path');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const autoprefixer = require('autoprefixer');
module.exports = {
  entry: './app/index.js',
  output: {
    path: path.resolve(__dirname, 'public_html'),
    filename: './js/bundle.js'
  },
  
  devServer: {
    contentBase: path.join(__dirname, 'public_html'),
    overlay: true,
    watchContentBase: true
  },
  
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          
        }
      },
      
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer({
                  browsers:['ie >= 7', 'last 21 version']
                })
              ],
              sourceMap: true
            }
          },
          'sass-loader'
        ]
      }
    ]
  },
  
  plugins: [
    new MiniCssExtractPlugin({
      filename: "./css/style.css"
    })
  ],
  
  optimization: {
    minimizer: [
      new UglifyJsPlugin(),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  
};
